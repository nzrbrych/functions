const getSum = (str1, str2) => {
  if(typeof str1 === 'string' && typeof str2 === 'string' && 
  str1.length === str1.split('').filter(el => !isNaN(el)).length &&
     str2.length === str2.split('').filter(el => !isNaN(el)).length){
      if(str1.length === 0 || str2.length === 0){
        return str1.length !== 0 ? str1 : str2
      }
      else{
        let resStr = ''
        for(let index = 0; index < str1.length; index++){
          resStr += parseInt(str1.split('')[index]) + parseInt(str2.split('')[index])
        }
        return resStr
      }
    }
    else return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let res = [0,0]
  for(let el of listOfPosts){
    if(el.author === authorName && el.post !== undefined){
      res[0] += 1
    }
    if(el.comments !== undefined){
      for(let innerEl of el.comments){
        if(innerEl.author === authorName && innerEl.comment !== undefined){
          res[1] += 1
        }
      }
    }
  }
  return 'Post:'+res[0]+',comments:'+res[1];
};

const tickets=(people)=> {
  let result = ''
  let reserve = 0
  for(let per of people){
    if(per === 25){
      reserve += per
      result = 'YES'
    } 
    else{
      if(reserve >= per - 25){
        reserve += per - 25
        result = 'YES'
      }
      else{
        result = 'NO'
        break
      }
    }
  }
  return result;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
